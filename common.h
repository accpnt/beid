#ifndef __COMMON_H__
#define __COMMON_H__

#define NAME      ("beid")
#define VERSION   (0)
#define REVISION  (2)
#define DESC      ("Beacon Embedded Interface Daemon")

#define YES  (1)
#define NO   (0)

/* generic return values */
#define RC_SUCCESS           (0)
#define RC_FAILURE           (1)



#endif  /* __COMMON_H__ */
