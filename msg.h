#ifndef __MSG_H__
#define __MSG_H__
	
#include <inttypes.h>

#define MSG_ID_SPY      (0x01)
#define MSG_ID_VERSION  (0x02)

struct msg_spy 
{
	uint8_t id;
	uint8_t spy;
};

struct msg_version
{
	uint8_t id;
	uint8_t name[4];
	uint8_t version;
	uint8_t revision;
};

int msg_handle_spy(struct msg_spy * spy,  char * sendmsg);

#endif  /* __MSG_H__ */