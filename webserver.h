#ifndef __WEBSERVER_H__
#define __WEBSERVER_H__
#include "conf.h"

int  webserver_init(struct conf * c);
void webserver_free(void);

#endif  /* __WEBSERVER_H__ */