#ifndef _TRANSPORT_H_
#define _TRANSPORT_H_
#include "conf.h"

int tr_init(struct conf *);
int tr_recv(void *, int);
int tr_send(void *, int);

#endif  /* _TRANSPORT_H_ */

