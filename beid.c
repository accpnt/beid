/*
 * beid : Beacon Embedded Interface Daemon - Command line
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#include "common.h"
#include "conf.h"
#include "listener.h"
#include "webserver.h"
#include "util.h"
#include "ini.h"        /* parser for *.ini configuration file */

/*----------------------------------------------------------------------------*/
struct conf c;

/*----------------------------------------------------------------------------*/
static void
usage(void)
{
	fprintf(stderr, "%s %d.%d - %s\n", NAME, VERSION, REVISION, DESC);
	fprintf(stderr, "usage: %s [-v] [-c <ini_file>]\n", NAME);
}

static void *
sighandler(void *arg)
{
	int       sig;
	sigset_t  signal_set;

	for (;;) {
		sigfillset(&signal_set);
		sigwait(&signal_set, &sig);

		switch(sig) { /* if we get this far, we've caught a signal */
		case SIGINT:
		case SIGTERM:
			d_printf(D_WARN, "caught SIGINT/SIGTERM, exiting...\n");
			pthread_exit(NULL);
		case SIGSEGV:
			d_printf(D_WARN, "SIGSEGV !!\n");
			break;
		default:
			break;
		}
	}
}

int main(int argc, char * argv[])
{
	int i;
	unsigned int cflag = RC_FAILURE;  /* assume invalid config by default */
	unsigned int rc = RC_FAILURE;     /* assume error by default */
	sigset_t   signal_set;
	pthread_t  sig_thread;

	/* Argument check */
	if(argc <= 1 || (argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h')) {
		usage();
		return 0;
	}

	 if (getuid() || geteuid()) {
		fprintf(stderr, "this program must be run as root.\n");
		exit(0);
	 }

	/* Parse command line options */
	for(i = 1; (i + 1 < argc) && (argv[i][0] == '-'); i++) {
		switch (argv[i][1]) {
		case 'v':           /* verbose */
			verbose = 10;
			break;
		case 'c':           /* read *.ini configuration file */
			d_printf(D_INFO, "parsing '%s'... \n", argv[++i]);

			if(ini_parse(argv[i], conf_ini_handler, &c) < 0) {
				fprintf(stderr, "ERROR\n");
			} else {
				cflag = RC_SUCCESS;
			}
			break;
		default:            /* something's wrong */
			usage();
			break;
		}
	}

	if (RC_SUCCESS == cflag) {   /* config file parsed successfully */
		sigfillset(&signal_set);  /* block all signals */
		pthread_sigmask(SIG_BLOCK, &signal_set, NULL);

		if (listener_init(&c) < 0)
			goto listener_failed;

		if (radio_init(&c) < 0)
			goto radio_failed;

		if (webserver_init(&c) < 0)
			goto webserver_failed;

		d_printf(D_CRITICAL, "beid started successfully\n");

		/* create the signal handling thread */
		if (pthread_create(&sig_thread, NULL, sighandler, NULL))
			goto sighandler_failed;

		/* waiting for the signal handling thread to terminate */
		pthread_join(sig_thread, NULL);

		rc = 0;  /* no error */
	}

sighandler_failed:
webserver_failed:
	webserver_free();
radio_failed:
	radio_free();
listener_failed:
	listener_free();
exit:
	d_printf(D_CRITICAL, "%s: exiting\n", __func__);
	return 0;
}
