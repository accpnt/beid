#include <stdio.h>


int main(int argc, char * argv[])
{
    printf("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"");
    printf("\"http://www.w3.org/TR/html4/strict.dtd\">");
    printf("<html>");
    printf("<head>");
    printf("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-15\">");
    printf("<title>Beacon Monitor</title>");
    printf("<link href=\"css/beacon.css\" rel=\"stylesheet\" type=\"text/css\">");
    printf("</head>");
    printf("<body>");

    printf("<div class=\"banner\">");
    printf("    <p>Beacon Monitor</p>");
    printf("</div>");
    printf("<div class=\"navigation\">");
    printf("    <a href=\"index.htm\">index</a>");
    printf("    <a href=\"discover.htm\">Discover</a>");
    printf("    <a href=\"refresh.htm\">Refresh</a>");
    printf("</div>");
    printf("<div class=\"content\">");
    printf("    <p>Some content</p>");
    printf("</div>");
    printf("</body>");
    printf("</html>");

    return 0;
}
