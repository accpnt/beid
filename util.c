#include <stdio.h>
#include <stdarg.h>

#include "util.h"

int verbose = 10;

/* debug printf, show info if global verbose >= verbosity */
void
d_printf(uint8_t verbosity, char *fmt, ...) {
    char buffer[MAXLEN];

    if (verbose >= verbosity ) {
        va_list args;
        va_start(args, fmt);
        vsnprintf(buffer, MAXLEN, fmt, args);
        fprintf(stderr, "%s", buffer);
        va_end(args);
    }
}