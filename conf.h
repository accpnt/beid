#ifndef __CONF_H__
#define __CONF_H__

#include <string.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <time.h>
#include <net/if.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>

#define MAXFILENAMELEN 256

#define SYSCONFDIR     "/etc/beid/"	/* default folder */
#define BEID_CONF_FILE  "beid.ini"	/* default file */
#define BEID_GLOBAL_CONF_FILE SYSCONFDIR BEID_CONF_FILE

struct conf {
   struct in_addr  beid_ip_addr;
   struct in_addr  bm_ip_addr;
   int             beid_udp_port;
   int             bm_udp_port;
   int             verbosity;
   int             resend_timeout;
   int             disable_sync;
   char            webserver_port[5];
};


int
conf_ini_handler(void * u, const char * s, const char * f, const char * v);

#endif  /* __CONF_H__ */
