# beid - Beacon Embedded Interface Daemon #

### What is beid ? ###

beid is a UNIX daemon that is intended to work along with bm for beacon monitoring purposes.

### What do I need to run beid ? ###

In order to run beid, you will need a Linux PC (it also works on Windows with Cygwin).

### What is the software architecture ? ###

beid is a multi-threaded application that also embeds a web server ([Mongoose](https://github.com/cesanta/mongoose)). The application is written in C. 

Here's a summary of the main threads :

Threads  | Description
---------|------------
listener | Listens for incoming UDP packets and treat them accordingly
webserver| Basic web server that displays basic beacon information

### Configuration ###

Configuration is done with a INI file. Look for "beid.ini" in the sources.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact