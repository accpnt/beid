#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <assert.h>

#include "transport.h"
#include "util.h"

/*
 * The CAE communicates with the Portal using a single socket, accessed
 * by various functions and modules. Here we define global & static
 * variables so that we don't have to initialize sockaddr_in structures
 * and read configuration structure each time we want to send a message.
 * This socket is defined only once, and is reached by sendto/recvfrom
 * function wrappers defined below.
 * The socket variables hereunder are initialized during startup by the
 * tr_init function.
 */
static unsigned int fd;
static struct sockaddr_in local_addr;
static struct sockaddr_in si_other;
static socklen_t sin_size;

/* this global variable indicates that the initialization has already
 * been done successfully */
static int tr_initialized = 0;

/*
 * Initialize socket and structures used for message exchanges between
 * beid and bm.
 * This function should be called during startup when synchronizing
 * with bm. Otherwise it'll be called when spawning listener
 * thread.
 */
int
tr_init(struct conf * c)
{
   int rc = -1;  /* assuming failure by default */
   struct timeval tv = { c->resend_timeout, 0 };  /* timeout */

   sin_size = sizeof(si_other);

   /* we check if socket initialization has already been done or not */
   if (0 < tr_initialized) {
      rc = 0;
      goto exit;
   }

   /* initializing socket */
   if ((fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
      perror("socket");
      goto exit;
   }

   /* should not block forever (needed for startup synchronization), so
    * we set a reception timeout on the socket */
   if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1) {
      perror("setsockopt");
      goto exit;
   }

   local_addr.sin_addr.s_addr = c->beid_ip_addr.s_addr;
   local_addr.sin_port        = htons(c->beid_udp_port);
   local_addr.sin_family      = AF_INET;
   memset(&local_addr.sin_zero, 0, sizeof(local_addr.sin_zero));

   /* this structure will only be used when sending messages */
   si_other.sin_family      = AF_INET;
   si_other.sin_port        = htons(c->bm_udp_port);
   si_other.sin_addr.s_addr = c->bm_ip_addr.s_addr;
   memset(&si_other.sin_zero, 0, sizeof(si_other.sin_zero));

   if (bind(fd, (struct sockaddr *) &local_addr, sizeof(struct sockaddr)) == -1) {
      perror("bind");
      goto exit;
   }

   rc = 0;
exit:
  d_printf(D_INFO, "%s: exiting (rc = %d)\n", __func__, rc);
  tr_initialized++;
  return rc;
}

/*
 * recvfrom wrapper, requires message and its length for receiving
 * UDP packets from the GUI. The socket is set to timeout via
 * setsockopt option SO_RCVTIMEO, so that it doesn't block forever
 * while called.
 */
int tr_recv(void * m, int size)
{
   int nr = 0;
   struct sockaddr_in peer_addr;
   socklen_t sin_size;

   assert(m);

   /*
    * Here the sockaddr_in given as argument will be filled with
    * data from the peer, we only receive from the Portal or from
    * the DHCP server so we should check
    */
   sin_size = sizeof(struct sockaddr_in);
   if ((nr = recvfrom(fd, m, size, 0, (struct sockaddr *) &peer_addr, &sin_size)) < 0) {
      if (!(errno == EWOULDBLOCK)) {
         perror("recvfrom");
      }
   }

   /*
    * Check if the messages comes from the Portal or the DHCP
    * server (localhost, 127.0.0.1)
    */
   else if (!(peer_addr.sin_addr.s_addr == si_other.sin_addr.s_addr) &&
            !(peer_addr.sin_addr.s_addr == 16777343)) {
   }

   return nr;  /* return number of bytes received */
}  /* tr_recv */

/*
 * sendto wrapper, requires message and its len for sending UDP packets
 * to the portal.
 */
int
tr_send(void * m, int size)
{
   int ns = 0;

   assert(m);

   /* here the sockaddr_in given as argument is the one filled when
    * intializing socket, as we only send packets to the portal */
   if ((ns = sendto(fd, m, size, 0,
                    (struct sockaddr *) &si_other,
                    sizeof(si_other))) == -1) {
      perror("sendto");
   }
   return ns;
}
