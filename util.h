#ifndef __UTIL_H__
#define __UTIL_H__

#include <arpa/inet.h>
#include <sys/types.h>
#include <limits.h>

#define MAXLEN LINE_MAX

#define D_CRITICAL 1
#define D_ERROR 2
#define D_WARN 5
#define D_INFO 10

extern int verbose;

void
d_printf(uint8_t verbosity, char *fmt, ...);

#endif  /* __UTIL_H__ */