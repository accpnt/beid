/*
 * beid
 * Reads a configuration file and populates a structure
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "common.h"
#include "util.h"

/*
 * should be used as a callback function for the *.ini parser
 * this will populate the configuration list
 */
int
conf_ini_handler(void * u, const char * s, const char * f, const char * v)
{
	char * end;
	unsigned int rc = RC_SUCCESS;
	struct conf * c;

	if(NULL == u) {
		goto error;
	}
	else {
		c = (struct conf *)u;
	}


	/* TODO : use the u * pointer */

#define MATCH_SECTION(x) (strcmp(s,x) == 0)
#define MATCH_FIELD(x)   (strcmp(f,x) == 0)
#define MATCH_VALUE(x)   (strcmp(v,x) == 0)

	if (MATCH_SECTION("beid")) {
		if(MATCH_FIELD("ip")) {
			inet_aton(v, &(c->beid_ip_addr));
		}
		else if(MATCH_FIELD("port")) {
			c->beid_udp_port = atoi(v);
		}
		else if(MATCH_FIELD("verbosity")) {
			c->verbosity = atoi(v);
		}
		else if(MATCH_FIELD("resend_timeout")) {
			c->resend_timeout = atoi(v);
		}
		else if(MATCH_FIELD("disable_sync")) {
			c->disable_sync = atoi(v);
		}
	}

	if (MATCH_SECTION("bm")) {
		if(MATCH_FIELD("ip")) {
			inet_aton(v, &(c->bm_ip_addr));
		}
		else if(MATCH_FIELD("port")) {
			c->bm_udp_port = atoi(v);
		}
	}

	if (MATCH_SECTION("webserver")) {
		if(MATCH_FIELD("port")) {
			memset(c->webserver_port, '\0', sizeof(c->webserver_port));
			strncpy(c->webserver_port, v, strlen(v));
		}
	}

error:
	return rc;
}
