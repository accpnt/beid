# Makefile for fb, updated Tue Nov 30 13:49:35 MET 2010
# Make the build silent by default
V =

ifeq ($(strip $(V)),)
        E = @echo
        Q = @
else
        E = @\#
        Q =
endif
export E Q

TARGET      := beid
SRCS        := beid.c conf.c ini.c util.c transport.c listener.c webserver.c server.c radio.c
OBJS        := ${SRCS:.c=.o} 

CC       = gcc
WARNINGS = -Wall -Wstrict-prototypes -pedantic
DEBUG    = -g
CFLAGS   = -O2 $(WARNINGS) $(DEBUG) -Wall -W -Wpointer-arith -Wbad-function-cast
LFLAGS   = -pthread -ldl
LIBS     = 

.PHONY: all clean distclean 
all:: ${TARGET} ${PLUGIN}

${TARGET}: ${OBJS} 
	$(E) "  LINK    " $@
	$(Q) ${CC} ${LFLAGS} -o $@ $^ ${LIBS} 

${OBJS}: %.o: %.c  
	$(E) "  CC      " $@
	$(Q) ${CC} ${CFLAGS} -o $@ -c $< 

clean:: 
	$(E) "  CLEAN"
	$(Q) -rm -f *~ *.o ${TARGET} ${PLUGIN}

distclean:: clean
