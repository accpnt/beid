#ifndef __SERVER_H__
#define __SERVER_H__
#include "conf.h"

int  server_init(int port);
void server_run(void);
void server_free(void);

#endif  /* __SERVER_H__ */