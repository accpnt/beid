#ifndef __RADIO_H__
#define __RADIO_H__
#include "conf.h"

int  radio_init(struct conf * c);
void radio_free(void);

#endif  /* __RADIO_H__	 */
