/*
 * radio module 
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "radio.h"
#include "transport.h"
#include "util.h"
#include "msg.h"

#define BUFSIZE  (128)  /* size of message buffer */

extern struct conf c;
static pthread_t radio;  /* __listener */

static int 
parse_msg(char * recvmsg, int recvsize, char * sendmsg)
{
	int      sendsize = 0;  
	int      rc = 0;        /* return code for ACK messages */

	struct msg_spy * spy;

	if ((recvsize < 1) || (recvmsg == NULL) || (sendmsg == NULL)) {
		d_printf(D_ERROR, "message too short to be parsed\n");
		goto error;
	}

	/* 
	 * checking 1st byte for a known message id
	 */
	switch ((unsigned char) recvmsg[0]) {
	case MSG_ID_SPY:
		d_printf(D_INFO, "radio << spy message received\n");
		break;
	default:
		d_printf(D_INFO, "radio << received unknown message (id: 0x%x)\n", (unsigned char) recvmsg[0]);
		sendsize = -1;
		break;
	}

	return sendsize;

error:
	d_printf(D_ERROR, "error");
	return -1;
}  /* parse_msg */

/*  
 * Main handler receives messages from PE or DHCPD call parse_msg() and
 * handle discarded messages
 */
static void *
__radio(void * arg)
{
	int  ns, nr;
	char recvbuf[BUFSIZE];
	char sendbuf[BUFSIZE];

	while (1) {
		memset(recvbuf, '\0', BUFSIZE);
		memset(sendbuf, '\0', BUFSIZE);

		/* tr_recv times out after a while */ 
		if ((nr = tr_recv(recvbuf, BUFSIZE)) > 0) {
			/* we have something to parse */
			if ((ns = parse_msg(recvbuf, nr, sendbuf)) > 0) {
				/* we have something to send */
				if ((ns = tr_send(sendbuf, ns)) == -1) {
					d_printf(D_ERROR, "listener: error sending message\n");
				}
				else
				{
					d_printf(D_INFO, "listener >> message sent\n");
				}
			} 
			else {
				if (ns == -1) {
					d_printf(D_ERROR, "listener: error parsing message\n");
				}
				if (ns == 0) {
					d_printf(D_INFO, "listener: nothing to send\n");
				}
			}
		}
	}
}  /* __radio */

/* 
 * This function will initialize the main socket and launch 
 * listener and probe threads 
 */
int 
radio_init(struct conf * c)
{
	int rc = -1;  /* assuming failure by default */

	/* launching listener thread */
	if (pthread_create(&radio, NULL, __radio, NULL)) {
		perror("pthread_create");
		goto exit;
	}

	rc = 0;  /* no errors */
exit:
	d_printf(D_INFO, "%s: exiting (rc = %d)\n", __func__, rc);
	return rc;
}  /* radio_init */

void 
radio_free(void)
{
	if (radio) {
		pthread_cancel(radio);
		d_printf(D_INFO, "%s: waiting for thread to terminate\n", __func__);
		pthread_join(radio, NULL);
		d_printf(D_INFO, "radio thread stopped.\n");
	}
}
