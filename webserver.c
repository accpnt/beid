/*
 * webserver module
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "server.h"
#include "webserver.h"
#include "util.h"

extern struct conf c;
static pthread_t webserver;  /* __webserver */

/*
 * Main handler
 */
static void *
__webserver(void * arg)
{
	while (1) {
		server_run();
	}
}  /* __webserver */

/*
 * This function will initialize the main socket and launch
 * webserver thread
 */
int
webserver_init(struct conf * c)
{
	int rc = -1;  /* andssuming failure by default */

	/* Create and configure the server */
	server_init(c->webserver_port);

	// Serve request. Hit Ctrl-C to terminate the program
	d_printf(D_INFO, "%s: starting on port %s\n", __func__, c->webserver_port);

	// Cleanup, and free server instance

	/* launching listener thread */
	if (pthread_create(&webserver, NULL, __webserver, NULL)) {
		perror("pthread_create");
		goto exit;
	}

	rc = 0;  /* no errors */
exit:
	d_printf(D_INFO, "%s: exiting (rc = %d)\n", __func__, rc);
	return rc;
}  /* webserver_init */

void
webserver_free(void)
{
	if (webserver) {
		pthread_cancel(webserver);
		d_printf(D_INFO, "%s: waiting for thread to terminate\n", __func__);
		pthread_join(webserver, NULL);
		d_printf(D_INFO, "webserver thread stopped.\n");
	}
}
