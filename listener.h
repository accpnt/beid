#ifndef _LISTENER_H_
#define _LISTENER_H_
#include "conf.h"


int  listener_init(struct conf * c);
void listener_free(void);
#endif  /* _LISTENER_H_ */
