/*
 * listener module 
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "listener.h"
#include "transport.h"
#include "util.h"
#include "msg.h"

#define BUFSIZE  (128)  /* size of message buffer */

extern struct conf c;
static pthread_t listener;  /* __listener */

/*
 * This function is called whenever a UDP message is received by the 
 * _listener thread. The 1st byte of the input buffer is checked to identify 
 * message type, inducing a particular treatment. 
 * In most cases, received messages imply a specific treatment on user data. 
 * If a reply is to be sent, this function will fill the output buffer and 
 * return the number of bytes to send. 
 * This function also commands the message list : some messages to be sent 
 * need to be acknowledged. They'll be stored and sent back to the Portal, 
 * until the parsing function receives a matching acknowledgement. This task 
 * is handled by the msglist module. 
 * 
 * Return values : 
 * n number of bytes of the generated message, if there's something to send 
 * 0 if there's nothing to send (acknowledgement messages mostly)
 * -1 on error
 */
static int 
parse_msg(char * recvmsg, int recvsize, char * sendmsg)
{
	int      sendsize = 0;  
	int      rc = 0;        /* return code for ACK messages */

	struct msg_spy * spy;

	if ((recvsize < 1) || (recvmsg == NULL) || (sendmsg == NULL)) {
		d_printf(D_ERROR, "message too short to be parsed\n");
		goto error;
	}

	/* 
	 * checking 1st byte for a known message id
	 */
	switch ((unsigned char) recvmsg[0]) {
	case MSG_ID_SPY:
		d_printf(D_INFO, "listener << spy message received\n");
		break;
	default:
		d_printf(D_INFO, "listener << received unknown message (id: 0x%x)\n", (unsigned char) recvmsg[0]);
		sendsize = -1;
		break;
	}

	return sendsize;

error:
	d_printf(D_ERROR, "error");
	return -1;
}  /* parse_msg */

/*  
 * Main handler receives messages from PE or DHCPD call parse_msg() and
 * handle discarded messages
 */
static void *
__listener(void * arg)
{
	int  ns, nr;
	char recvbuf[BUFSIZE];
	char sendbuf[BUFSIZE];

	while (1) {
		memset(recvbuf, '\0', BUFSIZE);
		memset(sendbuf, '\0', BUFSIZE);

		/* tr_recv times out after a while */ 
		if ((nr = tr_recv(recvbuf, BUFSIZE)) > 0) {
			/* we have something to parse */
			if ((ns = parse_msg(recvbuf, nr, sendbuf)) > 0) {
				/* we have something to send */
				if ((ns = tr_send(sendbuf, ns)) == -1) {
					d_printf(D_ERROR, "listener: error sending message\n");
				}
				else
				{
					d_printf(D_INFO, "listener >> message sent\n");
				}
			} 
			else {
				if (ns == -1) {
					d_printf(D_ERROR, "listener: error parsing message\n");
				}
				if (ns == 0) {
					d_printf(D_INFO, "listener: nothing to send\n");
				}
			}
		}
	}
}  /* __listener */

/* 
 * This function will initialize the main socket and launch 
 * listener and probe threads 
 */
int 
listener_init(struct conf * c)
{
	int rc = -1;  /* assuming failure by default */

	if (c->disable_sync) {
		/* initializing socket */
		if (tr_init(c) < 0) {
			goto exit;
		}
	}
	/* launching listener thread */
	if (pthread_create(&listener, NULL, __listener, NULL)) {
		perror("pthread_create");
		goto exit;
	}

	rc = 0;  /* no errors */
exit:
	d_printf(D_INFO, "%s: exiting (rc = %d)\n", __func__, rc);
	return rc;
}  /* listener_init */

void 
listener_free(void)
{
	if (listener) {
		pthread_cancel(listener);
		d_printf(D_INFO, "waiting for thread to terminate\n");
		pthread_join(listener, NULL);
		d_printf(D_INFO, "listener thread stopped.\n");
	}
}
